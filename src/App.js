import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import FormReg from './component/FormReg';

function App() {
  return (
    <div className="container">
        <FormReg/>
    </div>
  );
}

export default App;
