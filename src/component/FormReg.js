import { Button, Col, Form, FormGroup, Input, Label, Row } from "reactstrap";

const FormReg = () => {
    return (
        <>
            <Row>
                <Col className="text-center"><h1>Hồ sơ nhân viên</h1></Col>
            </Row>
            <Form className="row">
                <Row>
                    <Col xs='6'>
                        <FormGroup row>
                            <Label sm={2} >
                                Ho ten
                            </Label>
                            <Col sm={10}>
                                <Input name="name" placeholder="Ho ten" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Ngay sinh
                            </Label>
                            <Col sm={10}>
                                <Input name="name" placeholder="Ngay sinh" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                SDT
                            </Label>
                            <Col sm={10}>
                                <Input name="name" placeholder="SDT" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Gioi tinh
                            </Label>
                            <Col sm={10}>
                                <Input name="name" placeholder="Gioi tinh" type="text" />
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col xs='6' className="text-center">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSNPPYQRCLyeyObsjMwbp1LB3wumXPRTgM6Fw&usqp=CAU" />
                    </Col>
                </Row>
                <Row>
                    <FormGroup row>
                        <Label sm={1} >
                            Cong viec
                        </Label>
                        <Col sm={11}>
                            <textarea className="form-control" placeholder="Cong viec" rows="5"/>
                        </Col>
                    </FormGroup>
                </Row>
                <Row>
                    <Col className="text-end mt-3">
                        <Button color="primary" className="me-3">Chi tiet</Button>
                        <Button color="info">Kiem tra</Button>
                    </Col>
                </Row>
            </Form>
        </>
    );
};

export default FormReg;